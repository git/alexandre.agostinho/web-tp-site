# Context
Aujourd'hui, avec le dévloppement important de l'informatique, on dénombre entre
150 et 8000 languages différents selon les critères de recherche. Quand
quelqu'un va souhaité commancer à apprendre la programmation, il devient alors
difficile pour lui de s'orienter parmis tout ces languages possible. Autre cas,
un devloppeur qui doit dévlopper une application va se demander quel est le
meilleur language de programmation pour réaliser sa solution. L'idée du site web
est donc de soit faire découvrir un language de programmation à des débutants ou
bien de permettre à des professionels d'avoir un premier aperçut des languages
qu'il a à sa disposition.


## Personas

#### Marine Dupont - Etuduante en Histoire
                          Age:  19 ans
                   Profession:  Etudiante dans une faculté d'histoire
          Situation familiale:  célibataire, sans enfant, heberge chez ses parents en ville
            centre d'intérêts:  La litérature, les echecs, le volley-ball
    Relation à la technonogie:  Basse, sait utiliser les base d'un logiciel de
                                traitement de texte et naviguer et faire de simple recherches sur
                                internet.
    Objectif:
        Un devoir de recherche et redactions sur les origine de la programmation
    logiciel lui est demandé. Ne connait rien sur le dommaine informatique.
    Cherche un moyen d'obtenirs des infos générales, sans avoir à fouiller les
    moindre recoins d'internet.

#### Olivier Martin - Retraité
                          Age:  72 ans
                   Profession:  Retraité, à travaillé la plupart de sa vie en temps qu'ingenieur du dommaine aérospatial.
          Situation familiale:  marié, 3 enfants et 4 petits-enfants, héberge dans une maison à la campagne à Faramans.
            centre d'intérêts:  La technologie, les tours de magie, la science-fiction
    Relation à la technonogie:  Haute, à travaillé toute sa vie avec des outils
                                des dernières technologies existantes, a pu donc suivre cette evolution
                                technologique. Il connait de nombreuses bases dans la programmation car il
                                lui est arrivé de créé de petits logiciels embarqués pour son travail.
    Objectif:
        Pour son propre plaisir, il cherche à dévlopper un site web qui donnerais
    une carte du ciel en temps réel la nuit pour suivre la position des étoiles.
    Bien qu'il est déjà programmé de petite applications, il n'a jamais touché à
    la programmation d'un site internet. Il cherche donc un moyen de s'orienté
    vers les bon languages et commancer à les apprendres.

#### Nathan Dunnom - Maçon
                          Age:  38 ans
                   Profession:  Maçon, travail actuellement dans la construction d'un immeuble
          Situation familiale:  Concubinage, 1 enfants, vie avec sa fammille dans la banlieue de Lyon.
            centre d'intérêts:  sudoku, cuisiner, aller à la salle de sport
    Relation à la technonogie:  Moyenement haute, il utilise souvent son téléphone, sa télévision, il sait aussi utiliser des
                                apareils connectés, faire de bonne recherches sur internet et regler des paramêtres basiques.
    Objectif:
        Son fils est un grand fan de jeu vidéo et lui demende de l'aider à en fabriquer un.
      Il doit pour cela rechercher les bon outils necessaires.

## Maquette

#### Le titre de la page
Présent sur:
  - Toute les page

Disparaît quand on va au bas de la page.
Fond dégradé.
Affiche le nom du site + le nom de la page courrante.

#### Le Menu
Présent sur:
  - Toute les page

Possède:
  - 3 liens à gauche: "Menu Principal", "Galerie", "Données"
  - Un menu 'Hamborger' à droite avec les liens: "A propos", "Contact"

Reste dans une position sticky sur la page.
Gris, boutons blanc qui deviennent noir sur fond blanc avec animation.

#### Le Aside
Présent sur:
  - Toute les page

Possède:
  - Une partie réservée au liens externes
  - Une autre qui répertorie les dernière mise à jour du site

blanche, ecriture noir, un trai de bordure sous chanque article.

#### Les articles - Contenu principal
Présent sur:
  - la page principale
  - les pages d'article

blanche, ecriture noir, un trai de bordure sous chanque article.

#### Le Footer
Présent sur:
  - Toute les page

Possède:
  - le nom de l'autheur du site
  - l'addresse email du createur du site

Gris, ecriture noire, position fixed sur la page.

#### La page de Gallerie
Regroupe les photos et vidéos présaent sur le site

#### La page de Données
Un tableau ou tous les languages de programmation présentés sur le site y
sont répertoriés et des donnés y sont attribué.

#### Les articles
Une page présentant un language de programmation.

#### La page de contact
Une page ou on peut remplir un formulaire pour contacté l'équipe du site.

#### La page à propos
Une page résumant l'histoire et le but du site.